const protocol = require('http');

//Identify and Designate a "virtual point" where the network connection will start and end.
const port = 4000;


//Create a connection
protocol.createServer((req, res) => {
	//describe the response when the client interacts/communicates with the server.
	res.write(`Welcome to the Server`);
	res.end(); //to describe the end of the connection/transmission.

}).listen(port)
//we will use the listen() to bind the connection into the designated address

//create a response in the terminal confirming that the server is hosted and running on the designated port.
console.log(`Server is Running on port ${port}`);